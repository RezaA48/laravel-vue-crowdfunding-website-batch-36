<?php
trait Hewan{
    public $nama;
    public $jumlahkaki;
    public $keahlian;

    public function atraksi()
    {
    echo "{$this->nama} Menggunakan {$this->keahlian}";
    }
}



abstract class Fight{
    use hewan;
    public $attackPower;
    public $defensePower;
    public $darah;

    public function menyerang($hewan)
    {
        echo "{$this->name} sedang menyerang {$hewan->nama}";

        $hewan->diserang($this);
    }

    public function diserang($hewan)
    {
        echo "{$this->name} sedang diserang {$hewan->nama}";
        $this->darah = $this->darah - $hewan->attackPower / $this->defensePower;
    }

    public function getInfo()
    {
        echo "<br>";
        echo "Nama : ($this->nama)";
        echo "<br>";
        echo "Jumlah Kaki : ($this->jumlahkaki)";
        echo "<br>";
        echo "Keahlian : ($this->keahlian)";
        echo "<br>";
        echo "Keahlian : ($this->attackPower)";
        echo "<br>";
        echo "Keahlian : ($this->defensePower)";
        echo "<br>";
        echo "Keahlian : ($this->darah)";
        echo "<br>";
        $this->atraksi();
    }
    abstract public function getInfoHewan();
}

class Elang extends  Fight{
    public function __construct ($nama){
    $this->nama = $nama;
    $this->jumlahkaki = "2";
    $this->keahlian = "Terbang Tinggi";
    $this->attackPower = "10";
    $this->defensePower = "5";
    $this->darah = "50";
    }
    public function getInfoHewan()
    {
        echo "Jenis Hewan = Elang";
        this->getInfo();
    }
}

class Harimau extends  Fight{
    public function __construct ($nama){
    $this->nama = $nama;
    $this->jumlahkaki = "2";
    $this->keahlian = "Lari Cepat";
    $this->attackPower = "7";
    $this->defensePower = "8";
    $this->darah = "50";
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan = Harimau";
        this->getInfo();
    }
    
}

$elang = new Elang('Elang');
$harimau = new Harimau('Harimau');

?>