<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traints\UseUuid;


class otp_code extends Model
{
    use UseUuid;

    protected $fillable = [
        'otp', 'valid_until', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}
